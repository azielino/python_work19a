import sys
from definitions import manager, history_to_str, history_print

input_list = sys.argv
if not len(input_list) == 5:
    print("! Uruchom program w formie: zakup.py result <str identyfikator produktu> <int cena> <int liczba zakupionych>")
elif input_list[1] == "result":
    del input_list[1]
    input_list[0] = "zakup"
    action = str()
    while not action == "stop":
        action = input(f"Wpisz akcje (saldo, zakup, sprzedaz, stop): ")
        manager.execute(action)
    product = input_list[1]
    price = int(input_list[2])
    if price >= 0:
        product_quantity = int(input_list[3])
        if product_quantity > 0:
            manager.account -= product_quantity*price
            if manager.account < 0:
                manager.account += product_quantity*price
                print("! Brak gotowki na zakup towaru wpisanego przy uruchamianiu programu")
            else:
                if product in manager.warehouse:
                    manager.warehouse[product] += product_quantity
                else:
                    manager.warehouse[product] = product_quantity
                manager.history.append(input_list)
        else:
            print("! Zla ilosc przy uruchamianiu programu")
    else:
        print("! Ujemna cena przy uruchamianiu programu")
    history_str = history_to_str(manager.history)
    with open('result.txt', "w") as file:
        for item in history_str:
            file.write(item + "\n")
else:
    print("! Uruchom program w formie: zakup.py result <str identyfikator produktu> <int cena> <int liczba zakupionych>")
if manager.history:
    history_print()