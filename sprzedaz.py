import sys
from definitions import manager, history_to_str, history_print

input_list = sys.argv
if not len(input_list) == 5:
    print("! Uruchom program w formie: sprzedaz.py result <str identyfikator produktu> <int cena> <int liczba sprzedanych>")
elif input_list[1] == "result":
    del input_list[1]
    input_list[0] = "sprzedaz"
    action = str()
    while not action == "stop":
        action = input(f"Wpisz akcje (saldo, zakup, sprzedaz, stop): ")
        manager.execute(action)
    if manager.warehouse:
        product = input_list[1]
        product_quantity = int(input_list[3])
        if product in manager.warehouse and manager.warehouse[product] >= product_quantity:
            price = int(input_list[2])
            if price >= 0:
                if product_quantity > 0:
                    manager.account += product_quantity*price
                    manager.warehouse[product] -= product_quantity
                    if manager.warehouse[product] == 0:
                            del manager.warehouse[product]
                    manager.history.append(input_list)
                else:
                    print("! Zla ilosc przy uruchomieniu programu")
            else:
                print("! Ujemna cena przy uruchomieniu programu")
        else:
            print("! Brak Towaru. Zla ilosc przy uruchamianiu programu")
    history_str = history_to_str(manager.history)
    with open('result.txt', "w") as file:
        for item in history_str:
            file.write(item + "\n")         
else:
    print("! Uruchom program w formie: sprzedaz.py result <str identyfikator produktu> <int cena> <int liczba sprzedanych>")
if manager.history:
    history_print()