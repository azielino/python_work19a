import sys
from definitions import manager, history_to_str, history_print

input_list = sys.argv
if not len(input_list) >= 3:
    print("! Uruchom program w formie: magazyn.py result <str identyfikator produktu 1> <str identyfikator produktu 2> ...")
elif input_list[1] == "result":
    del input_list[1]
    input_list[0] = "magazyn"
    action = str()
    while not action == "stop":
        action = input(f"Wpisz akcje (saldo, zakup, sprzedaz, stop): ")
        manager.execute(action)        
    if manager.warehouse:
        with open('result.txt', "w") as file:
            file.write("")
        print("\nStan magazynowy dla wybranych towarow:")
        for i in range (1, len(input_list)):
            if input_list[i] in manager.warehouse:
                with open('result.txt', "a") as file:
                    file.write(f"{input_list[i]}: {manager.warehouse[input_list[i]]}\n")
            else:
                with open('result.txt', "a") as file:
                    file.write(f"{input_list[i]}: brak w magazynie\n")
        with open('result.txt', "r") as file:
            print(file.read())
        manager.history.append(input_list)
        history_str = history_to_str(manager.history)
        with open('result.txt', "w") as file:
            for item in history_str:
                file.write(item + "\n")
        history_print()
    else:
        print("\n! Magazyn jest pusty")
        history_str = history_to_str(manager.history)
        with open('result.txt', "w") as file:
            for item in history_str:
                file.write(item + "\n")
        history_print()
else:
    print("! Uruchom program w formie: magazyn.py result <str identyfikator produktu 1> <str identyfikator produktu 2> ...")