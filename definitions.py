class Manager:

    def __init__(self):
        self.actions = {}
        self.account = int()
        self.warehouse = {}
        self.history = []
        self.history_str = []

    def assign(self, name):
        def decorate(cb):
            self.actions[name] = cb
        return decorate

    def execute(self, name):
        if name not in self.actions:
            print("Action not defined")
        else:
            self.actions[name](self)


manager = Manager()

@manager.assign("saldo")
def action_saldo(manager):
    account_pay = int(input("Podaj kwote wplaty w groszach (jak wyplata to podaj kwote ujemna): "))
    manager.account += account_pay
    comment = input("Dodaj komentarz: ")
    if not comment:
        comment = "- Nie dodano komentarza -"
    manager.history.append(["saldo", account_pay, comment])

@manager.assign("zakup")
def action_zakup(manager):
    while True:
        product = input("Podaj identyfikator kupowanego produktu: ")
        price = input("Podaj cene jednostkowa w groszach: ")
        price = num_check(price)
        if price >= 0:
            product_quantity = input("Podaj liczbe sztuk: ")
            while not (product_quantity.isnumeric() and product_quantity != "0"):
                product_quantity = input("! Podaj liczbe sztuk: ")
            product_quantity = int(product_quantity)
            if product_quantity > 0:
                manager.account -= product_quantity*price
                if manager.account < 0:
                    manager.account += product_quantity*price
                    print("! Za malo gotowki")
                    break
                else:
                    if product in manager.warehouse:
                        manager.warehouse[product] += product_quantity
                    else:
                        manager.warehouse[product] = product_quantity
            else:
                print("! Zla ilosc")
                continue
        else:
            print("! Ujemna cena")
            continue
        break
    manager.history.append(["zakup", product, price, product_quantity])

@manager.assign("sprzedaz")
def action_sprzedaz(manager):
    if manager.warehouse:
        while True:
            print(f"Stan magazynu: {manager.warehouse}")
            product = input("Podaj identyfikator sprzedawanego produktu: ")
            if not product in manager.warehouse:
                print("! Nie ma takiego towaru w magazynie")
                continue
            price = input("Podaj cene jednostkowa w groszach: ")
            price = num_check(price)
            if price >= 0:
                product_quantity = input("Podaj liczbe sprzedawanych sztuk: ")
                while not (product_quantity.isnumeric() and product_quantity != "0"):
                    product_quantity = input("! Podaj liczbe sprzedawanych sztuk: ")
                product_quantity = int(product_quantity)
                if product_quantity <= manager.warehouse[product]:
                    manager.account += product_quantity*price
                    manager.warehouse[product] -= product_quantity
                    if manager.warehouse[product] == 0:
                        del manager.warehouse[product]
                    manager.history.append(["sprzedaz", product, price, product_quantity])
                    break
                else:
                    print("! Zla ilosc")
                    continue
            else:
                print("! Zla cena")
                continue
    else:
        print("! Magazyn jest pusty")

@manager.assign("stop")
def action_stop(manager):
    manager.history.append(["stop"])

def num_check(num):
    while not num.isnumeric():
        num = input("! Podaj cene jednostkowa w groszach: ")
    return int(num)

def history_print():
    print("\nWszystkie podane parametry w formie w jakiej zostaly pobrane:")
    with open('result.txt', "r") as file:
        print(file.read())

def history_to_str(h):
    h_str = list()
    for list_in_history in h:
        for item in list_in_history:
            h_str.append(str(item))
    return h_str