from os import read
import sys
from definitions import manager, history_to_str, history_print

input_list = sys.argv
if not len(input_list) == 4:
    print("! Uruchom program w formie: przeglad.py result <int zakres od> <int zakres do>")
elif input_list[1] == "result":
    del input_list[1]
    input_list[0] = "przeglad"
    action = str()
    while not action == "stop":
        action = input(f"Wpisz akcje (saldo, zakup, sprzedaz, stop): ")
        manager.execute(action)        
    if len(manager.history) < int(input_list[2]):
        print("\n! Podany zakres przekracza ilosc zapisanych akcji")
    else:
        manager.history.append(input_list)
        with open('result.txt', "w") as file:
            file.write(f"Zakres [{input_list[1]}, {input_list[2]}]:\n")
            for nr in range (int(input_list[1]), int(input_list[2])+1):
                for list_item in manager.history[nr]:
                    file.write(str(list_item) + "\n")
        print("\n")
        with open('result.txt', "r") as file:
            print(file.read())
    history_str = history_to_str(manager.history)
    with open('result.txt', "w") as file:
        for item in history_str:
            file.write(item + "\n")
    history_print()
else:
    print("! Uruchom program w formie: przeglad.py result <int zakres od> <int zakres do>")