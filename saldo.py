import sys
from definitions import manager, history_to_str, history_print

input_list = sys.argv
if not len(input_list) == 4:
    print("! Uruchom program w formie: saldo.py result <int wartosc> <str komentarz>")
elif input_list[1] == "result":
    del input_list[1]
    input_list[0] = "saldo"
    action = str()
    while not action == "stop":
        action = input(f"Wpisz akcje (saldo, zakup, sprzedaz, stop): ")
        manager.execute(action)        
    account_pay = int(input_list[1])
    manager.account += account_pay
    manager.history.append(input_list)
    history_str = history_to_str(manager.history)
    with open('result.txt', "w") as file:
        for item in history_str:
            file.write(item + "\n")
else:
    print("! Uruchom program w formie: saldo.py result <int wartosc> <str komentarz>")
if manager.history:
    history_print()