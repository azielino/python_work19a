import sys
from definitions import manager, history_to_str, history_print

input_list = sys.argv
if not len(input_list) == 2:
    print("! Uruchom program w formie: konto.py result")
elif input_list[1] == "result":
    action = str()
    while not action == "stop":
        action = input(f"Wpisz akcje (saldo, zakup, sprzedaz, stop): ")
        manager.execute(action)
    manager.history.append(["konto"])
    with open('result.txt', "w") as file:
        file.write(str(manager.account))
    print("\nStan konta:")
    with open('result.txt', "r") as file:
        print(file.read())
    history_str = history_to_str(manager.history)
    with open('result.txt', "w") as file:
        for item in history_str:
            file.write(item + "\n")
    history_print()